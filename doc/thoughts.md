**Q. What is a good way to determine the next free position???**  
**Answer:**  
Well, two methods come to mind.  
*Method 1:*
Instead of maintaining a single free_index integer, we could create a stack of integers to which we could push or pop free indicies when required.

The disadvantage of this stack method is that it occupies **too** much space and has **heavy** time penalties during initialization or expansion of the list, esp. as the list grows (since we'd need to push all of the new free indicies from the expanded space onto the stack - this grows exponentially).

*Method 2:*
Instead we can follow a more *dynamic* approach whereby we do the following:   

Alongside the free_index integer, we maintain another integer which we will call fresh_distance. Initially, it will be set to 0.

fresh_distance is essentially the number of old spaces/blocks that were freed up and should now be filled before we resume filling fresh (never before used) space.

Let's begin with a Memory and a single List. Let's also assume that we have only performed appeding/inserting operations so that the fresh_distance value will remain at 0.  

When we now delete an element from a list, we get new space but this is not "fresh space". So what we could do is move the free_index to this recently freed up space and have it's next pointer point to the previous "fresh free space" index that free_index was previously pointing to. Increment fresh_distance by 1.

Now if another deletion happens, giving us more free space, then we move free_index again and update next/prev and increase fresh_distance by 1.

Then let's say that we fill these two spaces and bring fresh_distance back to 0. Then from here on out, we know that we are at a fresh space and can increment the free_index by 3.

That is the reason for storing fresh_distance is to essentially just let's us know if we are in fresh terrain and if not how far we are from it. that while free_index is in fresh terrain, we can simply do +3 to get the next free position. But when we are in not-fresh space we need to use the "next" position to jump to the new free position.
