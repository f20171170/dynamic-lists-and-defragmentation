/* An assortment of functions that are useful to other functions. */
#include "../lib/helpers.h"


void updateFreeIndex(Memory* mem) {
  /* Find the next free index and set mem->free_index to it. */
  int next_free_index;

  if (mem->fresh_distance != 0) {
		next_free_index =  mem->start_ptr[mem->free_index+1];
    --mem->fresh_distance;
  }
  else
	  next_free_index =  mem->free_index + 3;

  if (next_free_index + 2 > mem->capacity)
    expandMemory(mem);

	mem->free_index = next_free_index;
}

int getValidListNumber(Memory* mem) {
  int list_no;
	printf("List number: ");
	scanf("%d", &list_no);
	list_no = list_no - 1;
	if(list_no < 0 || list_no >= mem->registry.filled) {
		printf("Out of bounds. Number of existing lists is: %d\n\n", mem->registry.filled);
		return -1;
	}
  return list_no;
}


int reorderReclaimedSpace(Memory* mem) {

  int i, j, k;  // multipurpose, re-usable variables.

  // Set up an array called reclaimed_indicies and copy over
  // the value of fresh distance so that we can quickly reference it.
  int fresh_distance = mem->fresh_distance;
  int reclaimed_indicies[fresh_distance];

  // Reclaiming space only exists if fresh_distance is not 0.
  // So check that first.
  if (fresh_distance == 0)
    return mem->free_index;

  // Now populate the reclaimed_indicies array
  j = mem->free_index; // j will act as a cursor
  reclaimed_indicies[0] = j;
  for(i=1; i<fresh_distance; ++i) {
    j = mem->start_ptr[j+1];
    reclaimed_indicies[i] = j;
  }

  // Create a variable to store where the fresh sector starts.
  // This value is very useful for defragmentation.
  int fresh_free_index =  mem->start_ptr[j+1];

  // Perform a simple insertion sort on the reclaimed_indicies array
  // to arrange the indicies in ascending order.
  for(i=0; i<fresh_distance; ++i) {
    for(j=i; j>0; --j) {
      if(reclaimed_indicies[j] < reclaimed_indicies[j-1]) {
        k = reclaimed_indicies[j-1];
        reclaimed_indicies[j-1] = reclaimed_indicies[j];
        reclaimed_indicies[j] = k;
      }
      else break;
    }
  }

  // Now point everything in the right direction
  // free_index -> lowest_reclaimed->highest_reclaimed->fresh_free_index
  for(i=0; i<fresh_distance-1; ++i) {
    j = reclaimed_indicies[i];
    k = reclaimed_indicies[i+1];
    mem->start_ptr[j+1] = k;
    mem->start_ptr[k+2] = j;
  }

  int bridge = reclaimed_indicies[mem->fresh_distance-1];
  mem->start_ptr[bridge + 1] = fresh_free_index;
  mem->start_ptr[fresh_free_index + 2] = bridge;
  mem->free_index = reclaimed_indicies[0];

  return fresh_free_index; // This value will be used by the defragmentation function.
}


/* Expansion Methods */
	void expandMemory(Memory* mem) {
		/* Dynamically resize the memory. */
		mem->capacity = mem->capacity*GROWTH_FACTOR + INCREMENT_FACTOR;
		mem->start_ptr = (int *)realloc(mem->start_ptr, sizeof(int)*mem->capacity);
	}

	void expandRegistry(Memory* mem) {
		/* Dynamically resize the lists registry. */
		mem->registry.capacity = mem->registry.capacity*GROWTH_FACTOR + INCREMENT_FACTOR;
		mem->registry.lists = (List **)realloc(mem->registry.lists, sizeof(List *)*mem->registry.capacity);
	}
