/* All of the non-helper functions that are used by the main program or in debugging. */
#include "../lib/functions.h"


List* newList(Memory* mem) {
	/* Create a new empty list in Memory mem. This will not actually occupy any
	of the Memory's array yet. */
	if (mem->registry.filled == mem->registry.capacity)
		expandRegistry(mem);
	List* list = (List *)malloc(sizeof(List));
	list->size = 0;
	list->head_index = -1;
	list->tail_index = -1;
	mem->registry.lists[mem->registry.filled++] = list;
	return list;
}


void insertNewNodeSorted(Memory* mem, int list_number, int val) {
	/* Insert a new node/value in acending order */
	List* list = mem->registry.lists[list_number];

	int new_node_index = mem->free_index;
	mem->start_ptr[new_node_index] = val; // Set value at next free index.

	// pop from the free stack before the next/prev pointers get modified.
	updateFreeIndex(mem);

	// Case 0: the list is empty.
	if (list->size == 0)
		list->head_index = list->tail_index = new_node_index;

	// Case 1: the new value/node becomes the new head.
	else if(val <= mem->start_ptr[list->head_index]) {
		mem->start_ptr[new_node_index + 1] = list->head_index; // Link next.
		mem->start_ptr[list->head_index + 2] = new_node_index; // Link back previous.
		list->head_index = new_node_index; // Make new_node index the new head
	}

	// Case 2: the new value/node becomes the new tail.
	else if(val >= mem->start_ptr[list->tail_index]) {
		mem->start_ptr[new_node_index + 2] = list->tail_index; // Link next.
		mem->start_ptr[list->tail_index + 1] = new_node_index; // Link back previous.
		list->tail_index = new_node_index; // Make new_node index the new tail
	}

	// Case 3: the new value is somewhere in between the head and the tail.
	else {
		int cursor = list->head_index;  // Cursor will be an index at all points.
		while(val > mem->start_ptr[cursor])
			cursor = mem->start_ptr[cursor + 1]; // Go to the next position.
		int next_index = cursor;
		int prev_index = mem->start_ptr[cursor + 2];
		mem->start_ptr[new_node_index + 1] = next_index; // Set new the node's next.
		mem->start_ptr[new_node_index + 2] = prev_index; // Set new the node's prev.
		mem->start_ptr[prev_index + 1] = new_node_index; // Set the prev's node's next to new node index.
		mem->start_ptr[next_index + 2] = new_node_index; // Set the next's node's prev to new node index.
	}

	++list->size;
	++mem->filled;
	return;
}


bool deleteElementFromList(Memory* mem, int list_number, int n) {
	/* Delete the nth element from the list of list_number in the mem memory space.
		NOTE: lists here are 1-indexed as that is the norm in CS F211. */
	List* list = mem->registry.lists[list_number];

	// Case 0: Out of bounds
	if (n < 1 || n > list->size) {
		printf("OutOfBounds Exception.\n\n");
		return false;
	}

	int new_free_space;
	int prev_free_space = mem->free_index;

	// Case 1: Delete the head
	if (n == 1) {
		new_free_space = list->head_index;
		list->head_index = mem->start_ptr[list->head_index + 1]; // move head to next
	}

	// Case 2: Delete the tail
	else if (n == list->size) {
		new_free_space = list->tail_index;
		list->tail_index = mem->start_ptr[list->tail_index + 2]; // move head to prev
	}

	// Case 3: Delete an element in between
	else {
		int cursor = list->head_index;	// start at the head
		for(int i=1; i<n; ++i)
			cursor = mem->start_ptr[cursor+1]; // find the nth element
		int next_index = mem->start_ptr[cursor+1];
		int prev_index = mem->start_ptr[cursor+2];
		mem->start_ptr[prev_index+1] = next_index; // reset the next of the previous
		mem->start_ptr[next_index+2] = prev_index;	// reset the previous of the next
		new_free_space = cursor;
	}

	// Set previous free space as the next of this newly freed node
	mem->start_ptr[new_free_space + 1] = prev_free_space;

	++mem->fresh_distance;
	--mem->filled;
	--list->size;
	mem->free_index = new_free_space;

	return true;
}


// TODO: Clean up this function (refactor) and fix any improper comments.
void defragment(Memory* mem) {
	/* Basically perform a consolidation (despite the name being defragment).
	I.E. remove any interstitial free space and maximize the terminal contiguous
	free space in memory. */

	int src_index, dst_index, temp;

	// Defragmentation is only possible if there is some interstitial free space.
	// interstitial free space only exists when the fresh_distance is not 0.
	if(mem->fresh_distance == 0){
		return; // already defragmented.
	}

	// Reorder the reclaimed space and then check to see if the terminal contiguous free
	// space starts earlier than the border between reclaimed space and fresh space.
	// This can happen when the one or more of the reclaimed free spaces are just
	// before the fresh contiguous block.
	int free_sector = reorderReclaimedSpace(mem);
	if(mem->start_ptr[free_sector-2] == free_sector){
		free_sector -= 3;
		--mem->fresh_distance;
		// Now start traversing back/along the reclaimed space while it is contiguous.
		while(mem->start_ptr[free_sector+2] == free_sector-3 && mem->fresh_distance != 0){
			free_sector -= 3;
			--mem->fresh_distance;
		}
	}

	// Now, we know the last non-interstitial occupied block is the one immediately
	// before the free sector. So we will move this block into the first available
	// interstitial block which is what mem->free_index points to thanks to
	// reorderReclaimedSpace(). Then we still need to find the next non-interstitial
	// occupied block. So we will do that after decrementing the fresh_distance and
	// updating the free_index, and we will repeat until there are not interstitial
	// blocks, that is, until mem->fresh_distance is 0.

	int prev, next;

	while(mem->fresh_distance != 0) {
		int next_free_index = mem->start_ptr[mem->free_index+1];

		src_index = free_sector-3;  // The block just before the free sector.
		dst_index = mem->free_index; // The next available interstitial block.

		// Copy over the entire block, ALSO update references of the previous and next nodes
		prev = mem->start_ptr[src_index+2];
		mem->start_ptr[prev+1] = dst_index; // Update the next of the previous to the new location.
		next = mem->start_ptr[src_index+1];
		mem->start_ptr[next+2] = dst_index; // Update the previous of the next to the new location.

		// If any lists depend on this block (such as their head or tail) then update them too.
		for(int l=0; l<mem->registry.filled; ++l) {
			List* li = mem->registry.lists[l];
			if (li->head_index == src_index)
				li->head_index = dst_index;
			if (li->tail_index == src_index)
				li->tail_index = dst_index;
		}

		for(int i=0; i<3; ++i) {
			temp = mem->start_ptr[dst_index+i];
			mem->start_ptr[dst_index+i] = mem->start_ptr[src_index+i];
			mem->start_ptr[src_index+i] = temp; // Though not required, write back the destination block's previous contents.
		}

		// Prep the new free segment
		mem->start_ptr[src_index+1] = next_free_index;

		--mem->fresh_distance; // Decrease the fresh_distance
		mem->free_index = next_free_index; // Update the free_index

		// the free_sector moves back by one block
		free_sector -= 3;
		if(mem->start_ptr[free_sector-2] == free_sector){
			free_sector -= 3;
			--mem->fresh_distance;
			// Now start traversing back/along the reclaimed space while it is contiguous.
			while(mem->start_ptr[free_sector+2] == free_sector-3 && mem->fresh_distance != 0){
				free_sector -= 3;
				--mem->fresh_distance;
			}
		}
	}

	// In the end the free_index should be the start of the free_sector
	mem->free_index = free_sector;
}

/* Display Methods */
	void printList(Memory* mem, int list_number) {
		/* Print a given list from Memory m */
		List* list = mem->registry.lists[list_number];
		int cursor = list->head_index;
		printf("List #%d: [", list_number+1);
		for(int c=list->size; c>1; --c) {
			printf("%d, ", mem->start_ptr[cursor]);
			cursor = mem->start_ptr[cursor + 1];
		}

		if(list->size != 0)
			printf("%d]\n", mem->start_ptr[cursor]);
		else
			printf("<empty>]\n");

		return;
	}

	void printFreeList(Memory* mem) {
		/*Print out all of the free locations in order. */
		int fresh_distance = mem->fresh_distance;
		int cursor = mem->free_index;
		int size = mem->capacity;

		printf("Free locations: ");

		while(fresh_distance != 0) {
			printf("%d ", cursor);
			cursor = mem->start_ptr[cursor+1];
			--fresh_distance;
		}

		while(cursor < size) {
			printf("%d ", cursor);
			cursor += 3;
		}

		printf("\n");
	}

	void memDump(Memory* mem) {
		/* Print out a lot of useful information about the memory. */
		int capacity = mem->capacity;

		printf("Filled:                 %3d\n", mem->filled);
		printf("Capacity:               %3d\n\n", capacity);
		printf("Registry Filled:        %3d\n", mem->registry.filled);
		printf("Registry Capacity:      %3d\n\n", mem->registry.capacity);
		printf("Current free_index:     %3d\n", mem->free_index);
		printf("Current fresh_distance: %3d\n\n", mem->fresh_distance);
		printf("Values:   ");
		for(int i=0; i<capacity; ++i)
			printf("%3d ", mem->start_ptr[i]);
		printf("\nIndicies: ");
		for(int i=0; i<capacity; ++i)
			printf("%3d ", i);
		printf("\n\n");
	}
