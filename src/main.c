#include "../lib/menu.h"

void load_test_fixture(Memory* mem) {
	newList(mem);
	int val;
	for(val=100; val<1100; val += 100)
		insertNewNodeSorted(mem, 0, val);
	// List #1: [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
	deleteElementFromList(mem, 0, 1); // 100
	deleteElementFromList(mem, 0, 3); // 400
	deleteElementFromList(mem, 0, 8); // 1000
	deleteElementFromList(mem, 0, 6); // 800
	// List #1: [200, 300, 500, 600, 700, 900]
	insertNewNodeSorted(mem, 0, 9001);
	// List #1: [200, 300, 500, 600, 700, 900, 9001]
	return;
}

int main(int argc, char** argv) {
	Memory* mem = newMemory();
	if(argc == 2 && !strcmp(argv[1], "--test")) {
		printf("Loaded test fixture as List #1.\n");
		load_test_fixture(mem);
	}
	driveMenu(mem);
	freeMemory(mem);
	return 0;
}
