/* Core Functionality: Initializing or finalizing Memory */
#include "../lib/core.h"

  Memory* newMemory() {
    /* Initialize a new, empty Memory.*/
  	Memory* mem = (Memory *)malloc(sizeof(Memory));
  	mem->start_ptr = (int *)malloc(sizeof(int)*MEMORY_BASE_SIZE);
  	mem->filled = 0;
  	mem->capacity = MEMORY_BASE_SIZE;
  	mem->free_index = 0;
  	mem->fresh_distance = 0;

  	mem->registry.filled = 0;
  	mem->registry.capacity = LISTS_REGISTRY_BASE_LISTS_CAPACITY;
  	mem->registry.lists = (List **)malloc(sizeof(List *)*LISTS_REGISTRY_BASE_LISTS_CAPACITY);

  	return mem;
  }

  void freeMemory(Memory* mem) {
    /* Free up all elements in Memory mem. */
  	free(mem->start_ptr);

  	int filled = mem->registry.filled;
  	for(int i = 0; i < filled; ++i)
  		free(mem->registry.lists[i]);
  	free(mem->registry.lists);

  	free(mem);
  }
