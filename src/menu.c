#include "../lib/menu.h"

void displayMenu() {
	printf("Choose an option:\n\t");
	printf(" 0. Exit\n\t");
	printf(" 1. Create new list\n\t");
	printf(" 2. Insert a new node in a given list in sorted order\n\t");
	printf(" 3. Delete an element from a given list\n\t");
	printf(" 4. Count total elements excluding free list\n\t");
	printf(" 5. Count total elements of a list\n\t");
	printf(" 6. Display all lists\n\t");
	printf(" 7. Display free list\n\t");
	printf(" 8. Preform defragmentation\n\t");
	printf(" 9. Display this help menu.\n");
}

void case1(Memory* mem){
  newList(mem);
  printf("New list added: #%d\n\n", mem->registry.filled);
}

void case2(Memory* mem) {
  int val, list_no;
  list_no = getValidListNumber(mem);
  if (list_no == -1)
    return;
  printf("Value: ");
  scanf("%d", &val);
  insertNewNodeSorted(mem, list_no, val);
  printf("Value successfully inserted!\n\n");
}

void case3(Memory* mem) {
	int val, list_no;
	list_no = getValidListNumber(mem);
	if (list_no == -1)
		return;
	printf("Index (1-indexed): ");
	scanf("%d", &val);
	bool results = deleteElementFromList(mem, list_no, val);
	if (results)
		printf("Value successfully deleted!\n\n");
}

void case4(Memory* mem) {
  printf("Total occupied space in memory: %d\n\n", mem->filled);
}

void case5(Memory* mem) {
  int list_no = getValidListNumber(mem);
	if (list_no == -1)
		return;
	printf("Size of list #%d: %d\n\n", list_no+1, mem->registry.lists[list_no]->size);
}

void case6(Memory* mem) {
  int end = mem->registry.filled;
  for(int i=0; i<end; ++i)
    printList(mem, i);
  printf("\n");
}

void case7(Memory* mem) {
  printFreeList(mem);
	printf("\n");
}

void case8(Memory* mem) {
	defragment(mem);
	printf("Successfully defragmented!\n\n");
}

void case9(Memory* mem) {
  displayMenu();
  printf("\n");
}

CASE cases[] = {case1, case2, case3, case4, case5, case6, case7, case8, case9, memDump};

void runExecutor(int c, Memory* mem) {
  cases[c](mem);
}

void driveMenu(Memory* mem) {
  int choice;
  case9(mem);
  while(true) {
    printf(">>> ");
    scanf("%d", &choice);
    if (choice == 0)
      return;
    if(choice < 1 || choice > 10) {
      printf("Did not recognize choice. Try again.\n\n");
			continue;
    }
    runExecutor(choice-1, mem);
  }
}
