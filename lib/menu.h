#include "functions.h"
#ifndef MENU

  #define MENU

  typedef void (*CASE)(Memory*);

  void displayMenu();
  void runExecutor(int c, Memory* mem);
  void driveMenu(Memory* mem);

#endif
