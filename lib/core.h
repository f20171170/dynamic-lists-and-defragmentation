#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#ifndef CORE

  #define CORE
	#define MEMORY_BASE_SIZE 30
	#define LISTS_REGISTRY_BASE_LISTS_CAPACITY 10
	#define GROWTH_FACTOR 2
	#define INCREMENT_FACTOR 3

	/* Structures */
	typedef struct {
		int size;
		int head_index;
		int tail_index; // this is to greatly increase the speed of appending new values
	} List;

	typedef struct {
		int filled;
		int capacity;
		List** lists;
	} ListRegistry;

	typedef struct {
		int* start_ptr;
		int filled;
		int capacity;
		int free_index;
		int fresh_distance;
		ListRegistry registry;
	} Memory;
	/* Each block in memory will be 3 spaces large and follow an organization
	of: key | next | prev */

	/* Initializer */
	Memory* newMemory();

	/* Finalizer */
	void freeMemory(Memory* mem);

#endif
