#include "helpers.h"
#ifndef FUNCTIONS

  #define FUNCTIONS

  List* newList(Memory* mem);
  void insertNewNodeSorted(Memory* mem, int list_number, int val);
  bool deleteElementFromList(Memory* mem, int list_number, int n);
  void defragment(Memory* mem);

  /* Display Methods */
  void printList(Memory* mem, int list_number);
  void printFreeList(Memory* mem);
  void memDump(Memory* mem);

#endif
