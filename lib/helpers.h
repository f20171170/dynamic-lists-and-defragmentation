#include "core.h"
#ifndef HELPERS

  #define HELPERS
  void updateFreeIndex(Memory* mem);
  int getValidListNumber(Memory* mem);
  int reorderReclaimedSpace(Memory* mem);
  void expandMemory(Memory* mem);
  void expandRegistry(Memory* mem);

#endif
