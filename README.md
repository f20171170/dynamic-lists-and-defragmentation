# DSA Assignment 2018-19 Spring Semester
***Completed by: Hemanth V. Alluri (f2017A7PS1170P)***

<hr>

### Requirements:
1. Unix/Linux based system
2. GNU gcc
3. Make

*To install requirements on Ubuntu 16.04 onwards:*
```
$ sudo apt-get install gcc
$ sudo apt-get install build-essential
```

<hr>

### Download and Build Instructions:
1. Clone this repository.
```
$ git clone https://github.com/Hypro999/DSA-Assignment.git
```
or
```
$ git clone git@github.com:Hypro999/DSA-Assignment.git
```

2. Simple run the following from within the cloned directory:
```
$ make main
```

3. To run the program execute:
```
$ bin/program
```

<hr>
